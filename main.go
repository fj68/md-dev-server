package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"text/template"

	"github.com/fsnotify/fsnotify"
	"github.com/jaschaephraim/lrserver"
	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
)

const version = "1.1.0"

type TemplateValue struct {
	Content string // Rendered markdown content
	Title   string // Title of the markdown file
}

const DefaultTemplate = `<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{.Title}}</title>
	<script defer src="http://localhost:35729/livereload.js"></script>
	<link rel='stylesheet' type='text/css' href='https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/4.0.0/github-markdown.min.css'>
</head>
<body>
	<main class="markdown-body">{{.Content}}</main>
</body>
</html>
`

func NewTemplate(path string) (*template.Template, error) {
	var t string
	if path == "" {
		t = DefaultTemplate
	} else if content, err := os.ReadFile(path); err != nil {
		return nil, err
	} else {
		t = string(content)
	}
	return template.New("template").Parse(t)
}

func (v *TemplateValue) ApplyTemplate(t *template.Template) ([]byte, error) {
	var out bytes.Buffer
	err := t.ExecuteTemplate(&out, "template", v)
	return out.Bytes(), err
}

func RenderFile(path string, md goldmark.Markdown, templ *template.Template) ([]byte, error) {
	source, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var buf bytes.Buffer
	if err = md.Convert(source, &buf); err != nil {
		return nil, err
	}

	v := &TemplateValue{Content: buf.String(), Title: path}
	return v.ApplyTemplate(templ)
}

func main() {
	var showHelp bool
	var port int
	var quiet bool
	var templPath string
	var showVersion bool
	flag.BoolVar(&showHelp, "help", false, "Show help")
	flag.IntVar(&port, "port", 3000, "Port of the server")
	flag.BoolVar(&quiet, "quiet", false, "Discard all logs")
	flag.StringVar(&templPath, "template", "", "Path to HTML template")
	flag.BoolVar(&showVersion, "version", false, "Show version")

	flag.Parse()

	if showVersion {
		fmt.Println(version)
		return
	}

	path := flag.Arg(0)
	if path == "" || showHelp {
		fmt.Println("Yet another live-reload server for markdown.\n")
		fmt.Println("md-dev-server [options] <file-path>\n")
		fmt.Println("Options:")
		flag.PrintDefaults()
		os.Exit(1)
		return
	}

	// Create Logger
	logger := log.New(os.Stdout, "[Info] ", log.LstdFlags)
	errlog := log.New(os.Stderr, "[Error] ", log.LstdFlags)

	if quiet {
		logger.SetOutput(io.Discard)
		errlog.SetOutput(io.Discard)
	}
	logger.Println("Logger created.")

	// Create template
	logger.Println("Create template.")
	templ, err := NewTemplate(templPath)
	if err != nil {
		errlog.Fatalln(err)
		return
	}

	// Create parser
	logger.Println("Create markdown parser.")
	md := goldmark.New(
		goldmark.WithExtensions(
			extension.GFM,
			extension.Typographer,
		),
		goldmark.WithParserOptions(
			parser.WithAutoHeadingID(),
		),
	)
	// First Render
	logger.Println("Initial rendering.")
	rendered, err := RenderFile(path, md, templ)
	if err != nil {
		errlog.Fatalln(err)
		return
	}

	// Create file watcher
	logger.Println("Create file watcher.")
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		errlog.Fatalln(err)
		return
	}
	defer watcher.Close()

	// Create and start LiveReload server
	logger.Println("Create live-reload server.")
	lr := lrserver.New(lrserver.DefaultName, lrserver.DefaultPort)
	lr.SetStatusLog(logger)
	lr.SetErrorLog(errlog)
	go lr.ListenAndServe()

	// Start goroutine that requests reload upon watcher event
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Op&fsnotify.Write == fsnotify.Write {
					logger.Println("modified: ", event.Name)
					var result []byte
					result, err = RenderFile(path, md, templ)
					if err != nil {
						errlog.Println(err)
					} else {
						rendered = result
						lr.Reload(event.Name)
					}
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				errlog.Println(err)
			}
		}
	}()

	// Add file to watcher
	logger.Println("File to watch: ", path)
	err = watcher.Add(path)
	if err != nil {
		errlog.Fatalln(err)
		return
	}

	// Start serving html
	http.HandleFunc("/", func(w http.ResponseWriter, _req *http.Request) {
		w.Write(rendered)
	})

	logger.Println("Start dev server...")
	logger.Printf("Open http://localhost:%d/", port)
	http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}
