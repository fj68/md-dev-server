# md-dev-server

Yet another live-reload server for markdown.

![Screenshot GIF](https://gitlab.com/fj68/md-dev-server/-/raw/main/screenshot.gif)

In the screenshot, [ttyd](https://github.com/tsl0922/ttyd) and [Vivaldi](https://vivaldi.com) browser's  [Tab Tiling](https://help.vivaldi.com/desktop/tabs/tab-tiling/) functionality is used to place both terminal and rendering result in one page.

## Features

 - Simple but enough
 - Zero configuration, but customizable
 - One executable, no dependency
 - GitHub Flavored Markdown by default

## Installation

Go to [Releases](https://gitlab.com/fj68/md-dev-server/-/releases).

## Usage

```sh
Yet another live-reload server for markdown.

md-dev-server [options] <file-path>

Options:
  -help
        Show help
  -port int
        Port of the server (default 3000)
  -quiet
        Discard all logs
  -template string
        Path to HTML template
  -version
        Show version
```

### Example

```sh
md-dev-server ./README.md
```

Then open [localhost:3000](http://localhost:3000/).

### How to live coding

```sh
md-dev-server --quiet README.md &   # Run quietly in background
vivaldi http://localhost:3000 &     # Open localhost:3000 in your favorite browser
micro README.md                     # Edit file in your favorite editor
```

Every time you save the file, browser will be reloaded with new content.

### How to control logging

With `--quiet` option, all logs are discarded.

In order to keep quiet but save logs in a file, use redirect:

```sh
md-dev-server README.md &> livereaload.log
```

Or, to enable both showing logs on screen and saving it to a file simultaniously, use `tee`:

```sh
md-dev-server README.md | tee livereload.log
```

To discard informational logs only, redirect stdout to `/dev/null`:

```sh
md-dev-server README.md > /dev/null
```

### How to customize template

By `-template` option, you can alternate HTML template using [text/template](pkg.go.dev/text/template).

#### Default template

```html
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>{{.Title}}</title>
  <script defer src="http://localhost:35729/livereload.js"></script>
  <link rel='stylesheet' type='text/css' href='https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/4.0.0/github-markdown.min.css'>
</head>
<body>
  <main class="markdown-body">{{.Content}}</main>
</body>
</html>
```

#### Template variables

| Variable   | Type   | Description                |
| ---------- | ------ | -------------------------- |
| `.Content` | String | Rendered markdown content  |
| `.Title`   | String | Title of the markdown file |

## Contribution

TBA

## License

[MIT License](LICENSE)
