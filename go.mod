module gitlab.com/fj68/md-dev-server

go 1.17

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jaschaephraim/lrserver v0.0.0-20171129202958-50d19f603f71
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/yuin/goldmark v1.4.1
	golang.org/x/sys v0.0.0-20211003122950-b1ebd4e1001c // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
)
